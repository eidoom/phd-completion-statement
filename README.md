# [phd-completion-statement](https://gitlab.com/eidoom/phd-completion-statement)
* Based on [pandoc-pdf-template](https://gitlab.com/eidoom/pandoc-pdf-template)
* [Live here](https://eidoom.gitlab.io/phd-completion-statement/document.pdf)
