# Progress
My PhD research is focussed on the calculation of QCD amplitudes at the precision frontier.
This presents the challenge of handling the algebraic and analytic complexity of higher-order expressions, and carefully regulating the infrared behaviour to cancel poles at each perturbative order.
I work with other members of Simon Badger's [JetDynamics group](http://personalpages.to.infn.it/~badger/jetdynamics/) to implement analytical expressions of these amplitudes into the public `C++` library `NJet`[@Badger:2012pg].
The results provide a vital ingredient for theoretical predictions of cross sections at the LHC in the search for deviations from the Standard Model.

We recently released a new version of `NJet` at <https://bitbucket.org/njet/njet> making the work discussed below available for immediate phenomenological application.

## Infrared limits
I am extending `NJet` to include a library of the various soft and collinear functions of partonic amplitudes.
These limits will provide the building blocks of counterterms to regulate infrared divergences at NNLO[@TorresBobadilla:2020ekr; @Magnea:2020trj; @Heinrich:2020ybq].
Many such regulatisation schemes are proposed, including:

* Antenna subtraction[@GehrmannDeRidder:2005cm] 
* $q_T$-slicing[@Catani:2007vq]
* Sector decomposition subtraction[@Czakon:2010td; @Boughezal:2011jf]
* N-jettiness slicing[@Gaunt:2015pea]
* Projection-to-Born subtraction[@Cacciari:2015jma]
* CoLoRFulNNLO[@DelDuca:2016ily] (closest to the common NLO Catani-Seymour scheme[@Catani:1996vz])
* Nested soft-collinear subtraction[@Caola:2017dug]
* Geometric infrared slicing[@Herzog:2018ily]
* Local analytic sector subtraction (Torino scheme)[@Magnea:2018hab]

These limits also offer a convenient way to validate new higher-order expressions by comparison of numerical evaluation in infrared limits of phase space to the appropriate limit functions and lower-order "reduced" amplitudes through a momentum mapping scheme.

## Amplitude neural networks for gluon fusion to diphoton plus jets
The gluon-initiated diphoton amplitudes offer an attractive playground for novel technology as they are loop-induced, presenting challenges for conventional cross-section techniques.
Diphoton final states are also relevant for interesting phenomenology such as probing the Higgs coupling[@Caola:2015wna].
I added numerical implementations of them for multiplicities 4--6 into `NJet` by summing over permutations of the existing all-gluons processes as in Ref.[@deFlorian:1999tp].

We completed analytic gluon fusion to diphoton plus jet ($gg\to\gamma\gamma g$) at one-loop.
We first calculated the amplitude using the permutation-sum trick.
We then obtained efficient analytical expressions, in particular using a rational parametrisation of the external kinematics through momentum twistors[@Hodges:2009hk] and reconstruction over finite fields[@Peraro:2019svx] to bypass intermediate algebraic complexity, and validated against the previous result.

For the six-point process ($gg\to\gamma\gamma gg$), it seems even efficient analytical expressions may not provide desirable evaluation times.
Therefore, we studied the feasibility of using neural networks to optimise the evaluation of the matrix element for cross-section calculations, following related work for leptonic colliders[@Badger:2020uow].
We used the analytical five-point result to train a neural network ensemble then fed it into the Monte Carlo event generator Sherpa[@Bothmann:2019yzt] and investigated its behaviour within a full hadronic simulation.
Learning from this, we trained a new model on the six-point numerical implementation and demonstrated its use in cross-section calculations[@Aylett-Bullock:2021hmo].
This method offers a performant way to run high multiplicity contributions, in particular radiative corrections, for event generator simulations where conventional techniques are prohibitively slow.

## Two-loop five-point full-colour amplitudes for phenomenology
Modern collider experiments demand increasing precision from theoretical predictions, pushing for NNLO contributions to $2\to3$ processes.
Such "industrialised" analytical computation of two-loop five-point QCD processes presents huge technical challenges.
We first need a basis of special functions offering fast and stable evaluation over phase space;
for massless scattering, the pentagon functions library[@Chicherin:2020oor] has recently become available.
Generating the amplitude via colour-ordered diagrams, we must reduce tensor integrals to a master integral basis which can be expressed in these special functions.
This requires constructing and solving a very large system of IBP identities.
Finally, we must obtain the coefficients in an efficient form.

We have been working on implementing the coefficients within `NJet` to construct the 't Hooft-Veltman finite remainders for various processes.
These generally provide the double virtual corrections.
We use direct analytic reconstruction of the amplitudes over finite fields at the special function level.


We completed the two-loop leading-colour pure-gluon five-point amplitude, with the partonic channels in progress.
We will publish this work when all channels are ready.

We also completed the full-colour two-loop $gg\to\gamma\gamma g$[@Badger:2021imn].
For $pp\to\gamma\gamma j$, this enters at N$^3$LO but the large gluonic PDF means it provides a dominant correction to NNLO.
While the colour structure is simpler then the gluonic case, the leading-colour contribution contains non-planar integrals, which are those with the highest complexity.

# Completion Plan
I will allow three months to work on my thesis and submit with the end of March.

My thesis will be organised roughly as:

* Introduction
	* Context
	* Motivation
	* Outline of content
* Theory
	* QCD/SM
	* Scattering amplitudes
* Phenomenology
	* Colliders
	* Cross sections
	* Event generator simulations
* Performant and stable computation
	* Some discussion of the challenges of computing these things
	* Floating point numbers, catastrophic cancellation
	* C++, templating, etc
	* FORM optimisation
	* Role of parallelisation, vectorisation, etc
* Amplitude technology
	* Fixed-order expansion
	* Colour decomposition
	* Spinor helicity
	* Momentum twistors
	* Finite field arithmetic
	* Integral reduction
* Infrared structure of QCD
	* Discuss IR amplitude factorisation
	* Subtraction schemes
	* Present the IR library
* Amplitude neural networks
	* Discussion of NNs
	* Discuss/present diphoton amplitudes
	* Present our ANN work
* Full-colour two-loop $2\to3$ amplitudes for NNLO
	* Discuss calculations
	* Present $gg\to\gamma\gamma j$ (including stability, etc)
	* Present $pp\to jjj$
* Conclusion
	* Summarise
	* Highlight achievements
	* Show how comes together: 2L virtuals, NN reals, IR counterterms
	* Reinforce message of introduction
