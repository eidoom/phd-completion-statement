#!/usr/bin/env bash

perl -0777p -e "
    1 while s|(title = \")((?!\\\href).*?)(\".*?eprint = \")(.*?)\"|\1\\\href\{https://arxiv.org/abs/\4\}\{\2\}\3\4\"|gs; 
    " \
    $1 >$2
